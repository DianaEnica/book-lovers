from flask.ext.wtf import Form
from wtforms import TextField, IntegerField, PasswordField, SubmitField, SelectField
from wtforms import validators
from wtforms.validators import DataRequired
from wtforms.fields import StringField
from wtforms.widgets import TextArea
from werkzeug.datastructures import MultiDict

class uploadISBNForm(Form):
	upload_isbn = SubmitField('Upload poza isbn')


class ContactForm(Form):
	nume = StringField(u'nume:')
	email = StringField(u'email:')
	telefon = StringField(u'telefon:')
	mesaj = StringField(u'mesaj:', widget=TextArea())
	submit = SubmitField('Upload poza isbn')	

	def reset(self):
		blankData = MultiDict([ ('csrf', self.generate_csrf_token()  ) ])
		self.process(blankData)


class ApiGetForm(Form):
	isbn = StringField(u'isbn:')
	titlu = StringField(u'titlu:')
	autor = StringField(u'autor:')
	editura = StringField(u'editura:')

	submit = SubmitField('Ok')	


class ApiPostForm(Form):
	# Required fields
	isbn = StringField(u'isbn:', [validators.DataRequired()])
	titlu = StringField(u'titlu:',  [validators.DataRequired()])
	pret = StringField(u'pret:',  [validators.DataRequired()])
	librarie_selector = SelectField(label = 'librarie:', choices = [('carturesti', 'Carturesti'), 
 		('librarienet', 'librarie.net')], id='librarie_selector')

	
	# Not necessarily required fields
	autor = StringField(u'autor:')
	editura = StringField(u'editura:')
	link_poza = StringField(u'link poza:')
	link_carte = StringField(u'link carte')

 	
	submit = SubmitField('Ok')	

