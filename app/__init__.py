from flask import Flask
from flask.ext.bootstrap import Bootstrap
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.sqlalchemy import SQLAlchemy
from settings import DB_PATH, UPLOAD_FOLDER

from flask_mail import Mail


app = Flask(__name__)
app.secret_key = "secret"


app.config.update(dict(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 587,
    MAIL_USE_TLS = True,
    MAIL_USE_SSL = False,
    MAIL_USERNAME = 'booklovers.contact@gmail.com',
    MAIL_PASSWORD = 'errorhaters',
))


# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'PNG', 'gif', 'png', 'PNG', 'jpg', 'JPG'])


# os.getenv('DATABASE_URL', 'sqlite:///app.sqlite')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///booklovers_final.db'
app.config['DEBUG'] = True

db = SQLAlchemy(app)
db.create_all()
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

mail = Mail(app)

Bootstrap(app)
from app import views

