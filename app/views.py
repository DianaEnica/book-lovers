# -*- coding: utf-8 -*-
from app import app, db
from flask import render_template, request, redirect, url_for
import api
from api import get_books, get_books_isbn, get_books_title, get_books_author, get_books_publishing
import ocr
from ocr import recognize_ISBN
from forms import uploadISBNForm, ContactForm, ApiGetForm, ApiPostForm
from app.models import Books_Main, Carturesti, Librarie_Min, Librarie_Max
from flask_mail import Message
from app import mail
from sqlalchemy import distinct
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import table, column, select, update, insert


import os
from werkzeug.utils import secure_filename

from settings import UPLOAD_FOLDER

# encoding=utf8
import sys
import re

reload(sys)
sys.setdefaultencoding('utf8')


@app.route('/', methods = ['GET', 'POST'])
def index():
    status = None
    error = None
    manual_ISBN = None

    FILENAME = "ISBN1.jpg"
    if request.method == 'POST':
        #Read ISBN from input
        manual_ISBN=request.form.get('manualISBNinput', None)
        manual_ISBN=re.sub('[^0-9]','',manual_ISBN)
        print manual_ISBN
        if  manual_ISBN!="":
            return redirect('/ocr_ISBN/' + manual_ISBN)
        #ISBN OCR if no input
        print "post here"
        f = request.files['file']
        if not allowed_file:
            error = 'Error! File type not allowed'
        elif not f:
            error = 'Error! Please choose file'
        if f and allowed_file(f.filename):
            print "current folder:"
            print os.path.dirname(os.path.abspath(__file__))

            FILE_PATH = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)),app.config['UPLOAD_FOLDER'], FILENAME))
            print FILE_PATH
            f.save(FILE_PATH)
            # f.save(UPLOAD_FOLDER + FILENAME);
            status = 'file uploaded successfully'
            return redirect('/ocr_ISBN/' + str(FILENAME))

    return render_template("index.html", methods = ['GET', 'POST'])



def get_recommended_image_links(rec):
    rec_img=[]
    if len(rec)>=1:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[0].isbn)
        if obj.count()>=1:
            rec_img.append(obj[0].img)
        else:
            rec_img.append(None)
    if len(rec)>=2:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[1].isbn)
        if obj.count()>=1:
            rec_img.append(obj[0].img)
        else:
            rec_img.append(None)
    if len(rec)>=3:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[2].isbn)
        if obj.count()>=1:
            rec_img.append(obj[0].img)
        else:
            rec_img.append(None)
    if len(rec)>=4:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[3].isbn)
        if obj.count()>=1:
            rec_img.append(obj[0].img)
        else:
            rec_img.append(None)
    return rec_img

def find_same_author_objects(author, initial_isbn):
    object_Carturesti = Carturesti.query.filter(Carturesti.author==author, Carturesti.isbn!=initial_isbn)
    rec=[]
    # Eliminare recomandari duplicate
    for item in object_Carturesti:
        if item not in rec:
            rec.append(item)

    if len(rec)>=1:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[0].isbn)
        if obj.count()>=1:
            if obj[0].price < rec[0].price:
                rec[0].price = obj[0].price
                rec[0].link = obj[0].link
    if len(rec)>=2:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[1].isbn)
        if obj.count()>=1:
            if obj[0].price < rec[1].price:
                rec[1].price = obj[0].price
                rec[1].link = obj[0].link
    if len(rec)>=3:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[2].isbn)
        if obj.count()>=1:
            if obj[0].price < rec[2].price:
                rec[2].price = obj[0].price
                rec[2].link = obj[0].link
    if len(rec)>=4:
        obj = Librarie_Min.query.filter(Librarie_Min.isbn==rec[3].isbn)
        if obj.count()>=1:
            if obj[0].price < rec[3].price:
                rec[3].price = obj[0].price
                rec[3].link = obj[0].link
    return rec



@app.route('/ocr_ISBN/<string:filename>', methods = ['GET', 'POST'])
def ocr_isbn(filename):


    status = None
    error = None
    manual_ISBN = None

    FILENAME = "ISBN1.jpg"
    if request.method == 'POST':
        print "post here"
        #Read ISBN from input
        manual_ISBN=request.form.get('manualISBNinput2', None)
        manual_ISBN=re.sub('[^0-9]','',manual_ISBN)
        print manual_ISBN
        if  manual_ISBN!="":
            return redirect('/ocr_ISBN/' + manual_ISBN)

        #ISBN OCR if no input

        f = request.files['file']
        if not allowed_file:
            error = 'Error! File type not allowed'
        elif not f:
            error = 'Error! Please choose file'
        if f and allowed_file(f.filename):
            print "current folder:"
            print os.path.dirname(os.path.abspath(__file__))

            FILE_PATH = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)),app.config['UPLOAD_FOLDER'], FILENAME))
            print FILE_PATH
            f.save(FILE_PATH)
            # f.save(UPLOAD_FOLDER + FILENAME);
            status = 'file uploaded successfully'
            return redirect('/ocr_ISBN/' + str(FILENAME))



    error = None
    if filename == "ISBN1.jpg":
        ISBN = recognize_ISBN(filename);

    if filename.isdigit():
        ISBN = int(filename)

    if ISBN == None:
        error = "ISBN could not be detected properly"

    ISBN = unicode(ISBN)
    print "ISBN:", ISBN
    # Price comparison
    good_price = None
    bad_price = None
    object_Carturesti = Carturesti.query.filter(Carturesti.isbn==ISBN)
    object_Librarie_Min = Librarie_Min.query.filter(Librarie_Min.isbn==ISBN)
    object_Librarie_Max = Librarie_Max.query.filter(Librarie_Max.isbn==ISBN)

    print object_Carturesti.count()
    if object_Carturesti.count() + object_Librarie_Min.count() + object_Librarie_Max.count() == 0:
        error = "Cartea nu a fost gasită în baza de date"
        return render_template("search.html", error=error)
    else:
        print "object Carturesti:", object_Carturesti[0]
        title = object_Carturesti[0].title
        print "titlu:", title
        author = object_Carturesti[0].author
        print "titlu:", author

    librarie_good = 'Carturesti'
    librarie_bad = 'librarie.net'

    link_img = None
    if object_Carturesti.count()>0:
        if object_Librarie_Min.count()>0:
            link_img = object_Librarie_Min[0].img
            if float(object_Carturesti[0].price.replace(',', '.')) < float(object_Librarie_Min[0].price.replace(',', '.')):
                good_price = object_Carturesti[0]
                bad_price = object_Librarie_Min[0]
                librarie_good = 'Carturesti'
                librarie_bad = 'librarie.net'
            else:
                good_price = object_Librarie_Min[0]
                bad_price = object_Carturesti[0]
                librarie_good = 'librarie.net'
                librarie_bad = 'Carturesti'
        else:
            good_price = object_Carturesti[0]
            librarie_good = 'Carturesti'
    else:
        if object_Librarie_Max.count()>0:
            link_img = object_Librarie_Max[0].img
            good_price = object_Librarie_Max[0]
            librarie_good = 'librarie.net'


    if author:
        same_author_objects = find_same_author_objects(author, ISBN)
        RecImgLinks = get_recommended_image_links(same_author_objects)
    if good_price:
        good_price=good_price.price
    if bad_price:
        bad_price=bad_price.price



    return render_template("search.html", title=title, author=author,
             good_price=good_price, bad_price=bad_price, isbn=ISBN,
             librarie_good=librarie_good, librarie_bad=librarie_bad, error=error,
             same_author_objects=same_author_objects, image_link=link_img, rec_img_links=RecImgLinks)


def allowed_file(filename):
    ALLOWED_EXTENSIONS = app.config['ALLOWED_EXTENSIONS']
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/contact', methods = ['GET', 'POST'])
def contact():
    form = ContactForm()
    emailerror = None
    nameerror = None
    mesajerror = None

    print "request method:", request.method
    if request.method == 'POST':
        if not form.email.data:
            emailerror = "Introduceti email-ul."
        if not form.nume.data:
            nameerror = "Introduceti numele."
        if not form.mesaj.data:
            mesajerror = "Introduceti mesajul."
        elif not nameerror and not emailerror and not mesajerror:
            print form.nume.data
            print form.email.data
            print form.telefon.data
            print form.mesaj.data

            msg = Message("Mesaj de la " + form.nume.data, sender=form.email.data,
                    recipients=["booklovers.contact@gmail.com"])
            msg.body = str(form.mesaj.data + "\n\n ---mesaj de pe website---" +
                    "\nnume: " + form.nume.data + "\nemail: " + form.email.data + "\ntelefon: " + form.telefon.data)

            # msg.html = "<b>testing</b>"
            mail.send(msg)
            form.reset()
            return render_template('contact.html', form=form, message="Mesajul a fost trimis cu succes. Multumim!")

    return render_template('contact.html', form=form, emailerror=emailerror, nameerror=nameerror)


@app.route('/detalii')
def detalii():
    return render_template('detalii.html')





############################
# API views                #
############################


@app.route('/api/get', methods = ['GET', 'POST'])
def api_get():
    form = ApiGetForm()
    data = ""
    url = ""

    if request.method == 'POST':
        if form.isbn.data:
            url = '/api/books/isbn/'
            data = form.isbn.data
        elif form.titlu.data:
            url = '/api/books/title/'
            data = form.titlu.data
        elif form.autor.data:
            url = '/api/books/author/'
            data = form.autor.data
        elif form.editura.data:
            url = '/api/books/publishing/'
            data = form.editura.data

        if data and url:
            return redirect(url + data)
####################TO DO########################
        #else
        #ShowERROR
#################################################
    return render_template('api_get.html', form=form)


@app.route('/api/post', methods = ['GET', 'POST'])
def api_post():
    form = ApiPostForm(request.form)
    data = ""
    url = ""

    if request.method == 'POST':
        if form.validate():
            print 'form validated'
            isbn =  form.isbn.data
            titlu = form.titlu.data
            autor = form.autor.data
            editura = form.editura.data
            pret = form.pret.data
            link_poza = form.link_poza.data
            link_carte = form.link_carte.data
            librarie_selector = form.librarie_selector.data

            print isbn,titlu, autor, editura, pret, link_poza, link_carte, librarie_selector
            if librarie_selector == 'carturesti':
                new_entry = Carturesti(title=titlu,author=autor,isbn=isbn,link=link_carte,editura=editura,price=pret)
                db.session.add(new_entry)
                db.session.commit()

            else:
                new_entry_min = Librarie_Min(title=titlu, isbn=isbn, link=link_carte, img=link_poza, price=pret)
                new_entry_max = Librarie_Max(title=titlu, isbn=isbn, link=link_carte, img=link_poza, price=pret)
                db.session.add(new_entry_min)
                db.session.add(new_entry_max)
                db.session.commit()

            return render_template('api_post.html', form=form,
                success="Book added succesfully to database")

        else:
            return render_template('api_post.html', form=form,
                error="Error. Please complete all required data")


    return render_template('api_post.html', form=form)



@app.route('/api')
def api():
    return render_template('api.html')


@app.route('/api/books', methods=['GET'])
def return_all_books():
    return get_books()

@app.route('/api/books/isbn/<string:book_isbn>', methods=['GET'])
def return_isbn_books(book_isbn):
    return get_books_isbn(book_isbn)

@app.route('/api/books/title/<string:book_title>', methods=['GET'])
def return_title_books(book_title):
    return get_books_title(book_title)

@app.route('/api/books/author/<string:book_author>', methods=['GET'])
def return_author_books(book_author):
    return get_books_author(book_author)

@app.route('/api/books/publishing/<string:book_publishing>', methods=['GET'])
def return_publishing_books(book_publishing):
    return get_books_publishing(book_publishing)
