from app import db

# CREATE TABLE books_main ( book_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
# title VARCHAR(100) NOT NULL, author VARCHAR(100), isbn VARCHAR(50), bar_code VARCHAR(50), editura VARCHAR(100) )
class Books_Main(db.Model):
    __tablename__ = 'books_main'
    book_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(100), nullable=False)
    author = db.Column(db.Unicode(100))
    isbn = db.Column(db.Unicode(50))
    bar_code = db.Column(db.Unicode(50))
    editura = db.Column(db.Unicode(100))

# CREATE TABLE books_carturesti ( book_id INTEGER NOT NULL, title VARCHAR(100) NOT NULL, author VARCHAR(100),
# isbn VARCHAR(50), link VARCHAR(100), editura VARCHAR(100), price VARCHAR(20), PRIMARY KEY (book_id) )
class Carturesti(db.Model):
    __tablename__ = 'books_carturesti'
    book_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(100), nullable=False)
    author = db.Column(db.Unicode(100))
    isbn = db.Column(db.Unicode(50))
    link = db.Column(db.Unicode(100))
    editura = db.Column(db.Unicode(100))
    price = db.Column(db.Unicode(20))

    @property
    def serialize(self):
        return {
            'title': self.title,
            'author': self.author,
            'isbn': self.isbn,
            'editura': self.editura,
            'link': self.link,
            'price': self.price
        }


# CREATE TABLE books_librarie_small ( book_id INTEGER NOT NULL, title VARCHAR(100) NOT NULL,
# isbn VARCHAR(50), link VARCHAR(100), price VARCHAR(20), img VARCHAR(100), PRIMARY KEY (book_id) )
class Librarie_Min(db.Model):
    __tablename__ = 'books_librarie_small'
    book_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(100), nullable=False)
    isbn = db.Column(db.Unicode(50))
    link = db.Column(db.Unicode(100))
    price = db.Column(db.Unicode(20))
    img = db.Column(db.Unicode(100))

    @property
    def serialize(self):
        return {
            'title': self.title,
            'isbn': self.isbn,
            'link': self.link,
            'price': self.price,
            'img': self.img
        }

# CREATE TABLE books_librarie_big ( book_id INTEGER NOT NULL, title VARCHAR(100) NOT NULL,
# isbn VARCHAR(50), link VARCHAR(100), price VARCHAR(20), img VARCHAR(100), PRIMARY KEY (book_id) )
class Librarie_Max(db.Model):
    __tablename__ = 'books_librarie_big'
    book_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(100), nullable=False)
    isbn = db.Column(db.Unicode(50))
    link = db.Column(db.Unicode(100))
    price = db.Column(db.Unicode(20))
    img = db.Column(db.Unicode(100))

    @property
    def serialize(self):
        return {
            'title': self.title,
            'isbn': self.isbn,
            'link': self.link,
            'price': self.price,
            'img': self.img
        }