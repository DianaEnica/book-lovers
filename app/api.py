from app import app, db
import json
from flask import jsonify, Response
from app.models import Books_Main, Carturesti, Librarie_Min, Librarie_Max
import sys
import re
import os
from sqlalchemy import distinct


##########################################
#  GET ME LIKE ONE OF YOUR FRENCH GIRLS  #
##########################################

'''GET ALL BOOKS'''
def get_books():
    object_Carturesti = Carturesti.query.distinct(Carturesti.isbn).group_by(Carturesti.isbn)
    object_Librarie_Min = Librarie_Min.query.distinct(Librarie_Min.isbn).group_by(Librarie_Min.isbn)
    object_Librarie_Max = Librarie_Max.query.distinct(Librarie_Max.isbn).group_by(Librarie_Max.isbn)
    json_Carturesti = [row.serialize for row in object_Carturesti]
    json_Librarie_Min = [row.serialize for row in object_Librarie_Min]
    json_Librarie_Max = [row.serialize for row in object_Librarie_Max]
    if len(json_Carturesti)>0:
        main_object = json_Carturesti
        if len(json_Librarie_Min)>0:
            main_object += json_Librarie_Min
        elif len(json_Librarie_Max)>0:
            main_object += json_Librarie_Max
    else:
        if len(json_Librarie_Max)>0:
            main_object = json_Librarie_Max
        elif len(json_Librarie_Min)>0:
            main_object = json_Librarie_Min
    return jsonify({'books': main_object})


'''GET BY ISBN'''
def get_books_isbn(book_isbn):
    book_isbn=unicode(book_isbn)
    book_isbn=re.sub('[^0-9]','',book_isbn)
    object_Carturesti = Carturesti.query.distinct(Carturesti.isbn).group_by(Carturesti.isbn).filter(Carturesti.isbn==book_isbn)
    object_Librarie_Min = Librarie_Min.query.distinct(Librarie_Min.isbn).group_by(Librarie_Min.isbn).filter(Librarie_Min.isbn==book_isbn)
    object_Librarie_Max = Librarie_Max.query.distinct(Librarie_Max.isbn).group_by(Librarie_Max.isbn).filter(Librarie_Max.isbn==book_isbn)
    json_Carturesti = [row.serialize for row in object_Carturesti]
    json_Librarie_Min = [row.serialize for row in object_Librarie_Min]
    json_Librarie_Max = [row.serialize for row in object_Librarie_Max]
    if len(json_Carturesti)>0:
        main_object = json_Carturesti
        if len(json_Librarie_Min)>0:
            main_object += json_Librarie_Min
        elif len(json_Librarie_Max)>0:
            main_object += json_Librarie_Max
    else:
        if len(json_Librarie_Max)>0:
            main_object = json_Librarie_Max
        elif len(json_Librarie_Min)>0:
            main_object = json_Librarie_Min

    return jsonify({'books': main_object})


'''GET BY TITLE'''
def get_books_title(book_title):
    book_title = unicode(book_title)
    book_title.replace("-"," ")
    book_title = book_title.lower()

    object_Carturesti = Carturesti.query.distinct(Carturesti.isbn).group_by(Carturesti.isbn)
    object_Librarie_Min = Librarie_Min.query.distinct(Librarie_Min.isbn).group_by(Librarie_Min.isbn)
    object_Librarie_Max = Librarie_Max.query.distinct(Librarie_Max.isbn).group_by(Librarie_Max.isbn)

    json_Carturesti = []
    for row in object_Carturesti:
        for rand in row.title.split(" "):
            if rand.lower() == book_title:
                json_Carturesti.append(row.serialize)
                break
    json_Librarie_Min = []
    for row in object_Librarie_Min:
        for rand in row.title.split(" "):
            if rand.lower() == book_title:
                json_Librarie_Min.append(row.serialize)

    json_Librarie_Max = []
    for row in object_Librarie_Max:
        for rand in row.title.split(" "):
            if rand.lower() == book_title:
                json_Librarie_Max.append(row.serialize)


    if len(json_Carturesti)>0:
        main_object = json_Carturesti
        if len(json_Librarie_Min)>0:
            main_object += json_Librarie_Min
        elif len(json_Librarie_Max)>0:
            main_object += json_Librarie_Max
    else:
        if len(json_Librarie_Max)>0:
            main_object = json_Librarie_Max
        elif len(json_Librarie_Min)>0:
            main_object = json_Librarie_Min

    return jsonify({'books': main_object})


'''GET BY AUTHOR'''
def get_books_author(book_author):
    book_author = unicode(book_author)
    book_author.replace("-"," ")
    book_author = book_author.lower()

    object_Carturesti = Carturesti.query.distinct(Carturesti.isbn).group_by(Carturesti.isbn)
    json_Carturesti = []
    json_Librarie_Min = []
    for row in object_Carturesti:
        for word in row.author.split(" "):
            if word.lower()==book_author:
                object_Librarie_Min = Librarie_Min.query.distinct(Librarie_Min.isbn).group_by(Librarie_Min.isbn).filter(Librarie_Min.isbn == row.isbn)
                for rand in object_Librarie_Min:
                    json_Librarie_Min.append(rand.serialize)
                json_Carturesti.append(row.serialize)


    if len(json_Carturesti)>0:
        main_object = json_Carturesti
        if len(json_Librarie_Min)>0:
            main_object += json_Librarie_Min
    else:
        if len(json_Librarie_Min)>0:
            main_object = json_Librarie_Min
    print len(json_Carturesti)
    print len(json_Librarie_Min)

    return jsonify({'books': main_object})

'''GET BY PUBLISHING'''
def get_books_publishing(book_publishing):
    book_publishing = unicode(book_publishing)
    book_publishing.replace("-"," ")
    book_publishing = book_publishing.lower()

    object_Carturesti = Carturesti.query.distinct(Carturesti.isbn).group_by(Carturesti.isbn)
    json_Carturesti = []
    json_Librarie_Min = []
    for row in object_Carturesti:
        for word in row.editura.split(" "):
            if word.lower() == book_publishing:
                    object_Librarie_Min = Librarie_Min.query.distinct(Librarie_Min.isbn).group_by(
                        Librarie_Min.isbn).filter(Librarie_Min.isbn == row.isbn)
                    for rand in object_Librarie_Min:
                        json_Librarie_Min.append(rand.serialize)
                    json_Carturesti.append(row.serialize)


    if len(json_Carturesti)>0:
        main_object = json_Carturesti
        if len(json_Librarie_Min)>0:
            main_object += json_Librarie_Min
    else:
        if len(json_Librarie_Min)>0:
            main_object = json_Librarie_Min
    print len(json_Carturesti)
    print len(json_Librarie_Min)

    return jsonify({'books': main_object})
