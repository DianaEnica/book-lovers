### Proiectul echipei Error Haters la competiția FiiCode.

Componența echipei:
------------------

Diana Marusic, 
Diana-Maria Enică,
Andrei Barbu,
Ștefan Iordache

Descriere
---------

Scopul aplicației web "Book Lovers" este de a încuraja tinerii să citească cât mai
mult. Astfel, folosind aplicația noastră, utilizatorii vor putea găsi cartea dorită
la cel mai bun preț.

Dacă unui cititor i-a plăcut foarte mult o carte a unui anumit autor, el o poate "scana",
iar aplicația noastră îi va sugera imediat alte titluri ale aceluiași autor.

Instrucțiuni de utilizare
-------------------------

Metoda I (prin recunoaștere optică):

  - Fă poză codului ISBN.
  - Fă crop pozei, astfel încât să se vadă doar ISBN-ul.
  - Apasă butonul "Upload poză ISBN".
  - Selectează poza dorită.
  - Apasă butonul "Caută cartea".

Metoda II (prin ISBN scris manual):

  - Apasă în zona de scriere aflată sub butonul "Upload poză ISBN".
  - Scrie ISBN-ul.
  - Apasă butonul "Caută cartea".

Manual de instalare
-------------------

Pentru a rula aplicatia Book Lovers de pe linux, urmeaza urmatorii pasi.


  1. Descarca proiectul de pe bitbucket.
  2. Instaleaza din terminal: $ pip install –r requirements.txt
  3. $ python run.py
  4. Utilizare placuta! :smile:

Tehnologii folosite
-------------------

Backend:

  * Phyton
  * Server - Flask Framework + alte librării (requirements.txt)

Baze de date:

  * SQLAlchemy

Frontend:

  * Bootstrap
  * HTML
  * CSS
  * JavaScript

Bug-uri/restricții (vor fi soluționate în versiunile ulterioare)
-----------------

  * Prețurile se pot actualiza doar prin crawling repetat, zilnic.
  * Diverse afișări incorecte de imagini/text în modul resposive și lipsa coperților de la Cărturești
  * Utilizarea librăriei Pillow 2.7 pe Windows (nu se poate face update la ultima versiune)



© Error Haters 2017